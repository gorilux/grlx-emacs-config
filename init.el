(setq utf-translate-cjk-mode nil) ; disable CJK coding/encoding (Chinese/Japanese/Korean characters)
(set-language-environment 'utf-8)
(set-keyboard-coding-system 'utf-8-mac) ; For old Carbon emacs on OS X only
(setq locale-coding-system 'utf-8)
(set-default-coding-systems 'utf-8)
(set-terminal-coding-system 'utf-8)
(unless (eq system-type 'windows-nt)
(set-selection-coding-system 'utf-8))
(prefer-coding-system 'utf-8)


(require 'package)
(add-to-list 'package-archives
         '("melpa" . "http://melpa.org/packages/") t)

(package-initialize)

(when (not package-archive-contents)
    (package-refresh-contents))

(unless (package-installed-p 'use-package)
  (package-install 'use-package))

(require 'use-package)
(setq use-package-always-ensure t)

(add-to-list 'load-path "~/.emacs.d/custom")

(require 'setup-general)
(if (version< emacs-version "24.4")
    (require 'setup-ivy-counsel)
  (require 'setup-helm))
;; (require 'setup-ggtags)
(require 'setup-cedet)
(require 'setup-editing)
(require 'moe-theme)




;; function-args
;; (require 'function-args)
;; (fa-config-default)
(define-key c-mode-map  [(tab)] 'company-complete)
(define-key c++-mode-map  [(tab)] 'company-complete)
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   (quote
    (0blayout bash-completion emr flycheck-clangcheck flycheck-irony cmake-mode cmake-project cmake-ide irony company-qml rtags powershell powerline moe-theme sr-speedbar ggtags zygospore helm-gtags helm yasnippet ws-butler volatile-highlights use-package undo-tree iedit dtrt-indent counsel-projectile company clean-aindent-mode anzu))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

(set-face-attribute 'default (selected-frame) :height 85)



;;(setq moe-theme-highlight-buffer-id t)

;; Resize titles (optional).
;;(setq moe-theme-resize-markdown-title '(1.5 1.4 1.3 1.2 1.0 1.0))
;;(setq moe-theme-resize-org-title '(1.5 1.4 1.3 1.2 1.1 1.0 1.0 1.0 1.0))
;;(setq moe-theme-resize-rst-title '(1.5 1.4 1.3 1.2 1.1 1.0))

;; Choose a color for mode-line.(Default: blue)
;(moe-theme-set-color 'cyan)
(moe-dark)

(require 'flycheck)
(require 'yasnippet)
(yas-global-mode 1)

;;;; RTAGS
;; ensure that we use only rtags checking
;; https://github.com/Andersbakken/rtags#optional-1
(defun setup-flycheck-rtags ()
  (interactive)
  (flycheck-select-checker 'rtags)
  ;; RTags creates more accurate overlays.
  (setq-local flycheck-highlighting-mode nil)
  (setq-local flycheck-check-syntax-automatically nil))

;; only run this if rtags is installed
(when (require 'rtags nil :noerror)
  ;; make sure you have company-mode installed
  (require 'company)
  (define-key c-mode-base-map (kbd "M-.")
    (function rtags-find-symbol-at-point))
  (define-key c-mode-base-map (kbd "M-,")
    (function rtags-find-references-at-point))
  ;; disable prelude's use of C-c r, as this is the rtags keyboard prefix
  ;; (define-key prelude-mode-map (kbd "C-c r") nil)
  ;; install standard rtags keybindings. Do M-. on the symbol below to
  ;; jump to definition and see the keybindings.
  (rtags-enable-standard-keybindings)
  ;; comment this out if you don't have or don't use helm
  (setq rtags-use-helm t)
  ;; company completion setup
  (setq rtags-autostart-diagnostics t)
  (rtags-diagnostics)
  (setq rtags-completions-enabled t)
  (push 'company-rtags company-backends)
  (global-company-mode)
  (define-key c-mode-base-map (kbd "<C-tab>") (function company-complete))
  ;; use rtags flycheck mode -- clang warnings shown inline
  (require 'flycheck-rtags)
  ;; c-mode-common-hook is also called by c++-mode
  (add-hook 'c-mode-common-hook #'setup-flycheck-rtags))


;;
(global-set-key (kbd "<f4>") 'ff-find-other-file)


;; cmake-ide

(require 'rtags) ;; optional, must have rtags installed
(cmake-ide-setup)


;; cmake-project

(defun maybe-cmake-project-hook ()
  (if (file-exists-p "CMakeLists.txt") (cmake-project-mode)))
(add-hook 'c-mode-hook 'maybe-cmake-project-hook)
(add-hook 'c++-mode-hook 'maybe-cmake-project-hook)

;; c style custom
;; style I want to use in c++ mode

(setq auto-mode-alist
      (append
       '(("\\.C$"    . c++-mode)
         ("\\.H$"    . c++-mode)
         ("\\.cc$"   . c++-mode)
         ("\\.hh$"   . c++-mode)
         ("\\.hpp$"  . c++-mode)
         ("\\.cpp$"  . c++-mode)
         ("\\.c$"    . c-mode)
         ("\\.h$"    . c++-mode)
         ("\\.m$"    . objc-mode)
         ("\\.java$" . java-mode)
         ("\\.cgi$"  . perl-mode)
         ("\\.pl$"   . perl-mode)
         ("\\.clj$"  . clojure-mode)
         ("\\.cljs$" . clojurescript-mode)
         ("\\.dfm$"  . c-mode)      ; For BCB forms, to get auto-revert
         ("\\.rb$" . robe-mode-map)
         ("\\.proto$" . protobuf-mode)
         ("Podfile\\'" . ruby-mode )
         ("\\.clp\\'" . clips-mode)
         ("\\.ino\\'" . arduino-mode)
         ("\\.vshader\\'" . glsl-mode)
         ("\\.fshader\\'" . glsl-mode)
         ("\\.md\\'" . markdown-mode)
         ("\\.markdown\\'" . markdown-mode)
         ("CMakeLists\\.txt\\'" . cmake-mode)
         ("\\.cmake\\'" . cmake-mode)
         ("\\.el\\'" . emacs-lisp-mode)
         ("\\.emacs\\'" . emacs-lisp-mode))
       auto-mode-alist))
;;

;; (c-add-style "work"
;;              '((indent-tabs-mode . nil)
;;                (c-basic-offset . 4)
;;                (c-offsets-alist
;;                 (substatement-open . 0)
;;                 (case-label . +)
;;                 (inline-open . 0)
;;                 (block-open . 0)
;;                 (statement-cont . +)
;;                 (inextern-lang . 0)
;;                 (innamespace . 0))))
;; style I want to use in c++ mode

(defconst my-cc-style
  '("cc-mode"
    (c-basic-offset . 4)

    (c-hanging-braces-alist     . ((substatement-open after)
                                   (defun-open after)
                                   (class-open)
                                   (class-close)
                                   (inline-open after)
                                   (block-open after)
                                   (brace-list-open after)
                                   (extern-lang-open after)
                                   (namespace-open after)))
    ;; Controls the insertion of newlines before and after certain colons.
    (c-hanging-colons-alist     . ((member-init-intro before)
                                   (inher-intro)
                                   (case-label after)
                                   (label after)
                                   (access-label after)))

    (c-offsets-alist . ((func-decl-cont . ++)
                        (member-init-intro . +)
                        (inher-intro . ++)
                        (comment-intro . 0)
                        (arglist-close . c-lineup-arglist)
                        (topmost-intro . 0)
                        (block-open . 0)
                        (inline-open . 0)
                        (substatement-open . 0)
                        (statement-cont . ++)
                        (label . /)
                        (case-label . +)
                        (statement-case-open . 0)
                        (statement-case-intro . 0) ; case w/o {
                        (access-label . -)
                        (inextern-lang . 0)
                        (innamespace . [0])))))


(c-add-style "my-cc-mode" my-cc-style)

(defun my-c++-mode-hook ()
  (c-set-style "my-cc-mode")        ; use my-style defined above
  (auto-fill-mode)
  (c-toggle-auto-hungry-state 1))

(defadvice c-lineup-arglist (around my activate)
  "Improve indentation of continued C++11 lambda function opened as argument."
  (setq ad-return-value
        (if (and (equal major-mode 'c++-mode)
                 (ignore-errors
                   (save-excursion
                     (goto-char (c-langelem-pos langelem))
                     ;; Detect "[...](" or "[...]{". preceded by "," or "(",
                     ;;   and with unclosed brace.
                     (looking-at ".*[(,][ \t]*\\[[^]]*\\][ \t]*[({][^}]*$"))))
            0                           ; no additional indent
          ad-do-it)))                   ; default behavior



(defvar my-cpp-other-file-alist
  '(("\\.cpp\\'" (".hpp" ".h"))
    ("\\.h\\'" (".hpp" ".cpp"))
    ("\\.hpp\\'" (".h" ".cpp"))
    ("\\.cxx\\'" (".hxx" ".ixx"))
    ("\\.ixx\\'" (".cxx" ".hxx"))
    ("\\.hxx\\'" (".ixx" ".cxx"))
    ("\\.c\\'" (".h"))
    ("\\.h\\'" (".c"))
    ))

(setq-default ff-other-file-alist 'my-cpp-other-file-alist)



(add-hook 'c++-mode-hook 'my-c++-mode-hook)
(global-linum-mode t)
