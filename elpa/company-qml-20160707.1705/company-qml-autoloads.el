;;; company-qml-autoloads.el --- automatically extracted autoloads
;;
;;; Code:
(add-to-list 'load-path (directory-file-name (or (file-name-directory #$) (car load-path))))

;;;### (autoloads nil "company-qml" "company-qml.el" (22517 26282
;;;;;;  529011 312000))
;;; Generated autoloads from company-qml.el

(autoload 'company-qml "company-qml" "\
A `company-mode' completion back-end for qml-mode.

\(fn COMMAND &optional ARG &rest IGNORED)" t nil)

;;;***

;;;### (autoloads nil nil ("company-qml-pkg.el" "qmltypes-parser.el"
;;;;;;  "qmltypes-table.el") (22517 26282 559011 315000))

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; End:
;;; company-qml-autoloads.el ends here
